//#include <avr>
//#include <interrrupts>
#include <avr/interrupt.h>
#include <avr/io.h> 
#include <EEPROM.h>

//#define PLOTTER

#define CONTROL_LOOP_DELAY 50000
#define SERIALIO_LOOP_DELAY 500000
#define MAX 0x5FFF
#define MAX_F 160.0
//#define TIMER_F 1.6e-5
#define TIMER2_MAX 100
#define TIMER2_MAX_F 100.0
#define TIMER_F (TIMER2_MAX_F/16000000.0)
word capture,old=0;
volatile word periodcount; 
volatile word periodcount_old;
volatile word T, T_desi, T_desi_1, T_desi_2;
long present;
int pwmsetting;
int speedup;
float P=0,I=0,D=0,E=0;
float Ti=1,Td=1,Kp=1,Tk=1;
int readcount,command;
float freq, ref=100, delta, delta_old;
#define MAX_CMD_SIZE 50



void serialio();
void controller();
void decode_command(char *serial_command_buf);
void serial_write_status();
void serial_write_plotter();
void store_param();
void restore_param();

ISR(TIMER1_OVF_vect){
    //TIFR1 = 0x1 << TOV1;
    //periodcount++;
}


ISR(TIMER2_OVF_vect){
    //TIFR1 = 0x1 << TOV1;
    periodcount++;
}

ISR(TIMER2_COMPA_vect){
    //TIFR1 = 0x1 << TOV1;
    periodcount++;
}

//ISR(TIMER2_COMPA_vect, ISR_ALIASOF(TIMER2_OVF_vect));

ISR(TIMER1_CAPT_vect){
    digitalWrite(13,HIGH);
    capture = ICR1L;
    capture |= ICR1H << 8;
    T_desi_1 = (MAX - old) + (capture);
    old = capture;
    T_desi = T_desi_1+T_desi_2;
    T_desi_2 = T_desi_1;
    T = periodcount_old+periodcount;
    periodcount_old = periodcount;
    periodcount = 0;
    digitalWrite(13,LOW);
}

void setup(){
  pinMode(13,OUTPUT);
  pinMode(10,OUTPUT);
  //timer 1 - PWM output
  TCCR1A = 0x83; //10bit fast pwm and non-inverting mode
  TCCR1A |= 3 << WGM10
          | 1 << COM1B1
          | 0 << COM1B0;
  TCCR1B = 0x3 << WGM12 //Mode
         | 0x1 << ICNC1 //noise cancelling
         | 0x01 << CS10;//clock source 5= F/1024
  OCR1AH = (byte)((MAX) << 8);
  OCR1AL = (byte)((MAX));
  DDRB = 1 << 1
       | 1 << 2;
  PORTB = 0x1 << 0;
  TIMSK1 = 0x0 << TOIE1
         | 0x1 << ICIE1;
  pwmsetting = 0x00FF;
  
  //timer0
  TCCR2A = 2 << WGM20;//1 << CS20; //clock select 
  TCCR2B = 1 << CS20;//1 << CS20; //clock select 
  TIMSK2 = (1 <<  TOIE2) | (1 << OCIE2A);// //overflow interrupt
  OCR2A = byte(TIMER2_MAX);
  
  I = 0.1;
  restore_param();
  Serial.begin(9600);
  
  
}

void loop(){
    static unsigned long now = 0;
    static unsigned long
      serial_next = 500,
      controller_next = 50;
    
    now = micros();
    if(now > controller_next){
      controller();
      controller_next += CONTROL_LOOP_DELAY;
    }
    if(now > serial_next){
      serialio();
      serial_next += SERIALIO_LOOP_DELAY;
    }
}


void controller(){
    present = ((long)T)<<11 + (long)T_desi;
    freq = 2/(float(T)*TIMER_F);
    delta = ref-freq;
    
    I += Kp*delta*CONTROL_LOOP_DELAY/10e+6/Ti;
    if(I < -0) I = -0;
    if(I > 1) I = 1;
    if(I != I) I = 0;
    
    D = Kp*(delta-delta_old)/CONTROL_LOOP_DELAY*10e+6*Td;
    delta_old = delta;
    if(D < -1) D = -1;
    if(D > 1) D = 1;
    if(D != D) D = 0;
    
    P = Kp*delta;
    if(P < -1) P = -1;
    if(P > 1) P = 1;
    //E = ref/MAX_F;
    
    pwmsetting = word((I+P+D)*float(MAX));
    if(pwmsetting < 0) pwmsetting=0;
    if(pwmsetting >= MAX) pwmsetting=MAX-1;
    OCR1BH = (int)(pwmsetting >> 8);
    OCR1BL = (int) pwmsetting >> 0;

}

void serialio(){
    static int serial_command_buf_idx=0;
    static char serial_command_buf[MAX_CMD_SIZE];
    
    float * changeparam;
  
    #ifdef PLOTTER
    serial_write_plotter();
    #else
    serial_write_status();
    #endif
    
    if(readcount>0) speedup=10;
    else speedup=1;
    
    readcount = Serial.available();
    while(readcount--){
      command = Serial.read();
      switch(command){
        case 0x2B:
          ref += speedup;
          break;
        case 0x2D:
          ref -= speedup;
          break;
        case 0x30:
        case 0x31:
        case 0x32:
        case 0x33:
        case 0x34:
        case 0x35:
        case 0x36:
        case 0x37:
        case 0x38:
        case 0x39:
        case 'R':
        case 'P':
        case 'I':
        case 'D':
        case '.':
          serial_command_buf[serial_command_buf_idx] = command;
          serial_command_buf_idx++;
          break;
        case 0xA:
        case 0xD:
        case 0x21:
          serial_command_buf[serial_command_buf_idx] = 0x00;
          decode_command(serial_command_buf);
          serial_command_buf_idx = 0;
          break;
      }
      if(serial_command_buf_idx > MAX_CMD_SIZE) serial_command_buf_idx = 0;
    }
}

void serial_write_plotter(){
  Serial.println(freq);
}
void serial_write_status(){
    Serial.print(freq);
    Serial.print(";");
    Serial.print(ref,1);
    Serial.print("\t");
    Serial.print(P,6);
    Serial.print(":");
    Serial.print(I,6);
    Serial.print(":");
    Serial.print(D,6);
    Serial.print("\t");
    Serial.print(Kp,6);
    Serial.print(":");
    Serial.print(Ti,6);
    Serial.print(":");
    Serial.print(Td,6);
    Serial.print(":");
    Serial.print((I+P+D)*float(MAX)); 
    Serial.print("@");
    Serial.print(pwmsetting);
    Serial.print("\r\n");
}

void decode_command(char serial_command_buf[20]){
      int state=0;
      float mult;
      int serial_command_buf_idx = 0;
      float * param;
      //Serial.print((char)serial_command_buf[0]);
      switch(serial_command_buf[serial_command_buf_idx++]){
        case 'P':
            param = &Kp;
            break;
        case 'I':
            param = &Ti;
            break;
        case 'D':
            param = &Td;
            break;
       case 'R':
            param = &ref;
            //Serial.println("R");
            break;
      }
      
      Serial.println(*param,2);
      *param = 0;
      mult = 1.0;
      state=0;
      while(serial_command_buf[serial_command_buf_idx]){
        if(serial_command_buf[serial_command_buf_idx] == '.'){
          state = 1;
          serial_command_buf_idx++;
          continue;
        }
        switch(state){
          case 0:
            *param *= 10;
            *param += (serial_command_buf[serial_command_buf_idx] - 0x30);
            break;
          case 1:
            mult *= 10;
            *param += (serial_command_buf[serial_command_buf_idx] - 0x30)/mult;
            //Serial.println(mult,10);
            break;
        }
        serial_command_buf_idx++;
      }
      store_param();
}

void store_param(){
    unsigned int addr=0;
    EEPROM.put(addr,ref);
    addr += sizeof(ref);
    EEPROM.put(addr,Kp);
    addr += sizeof(Kp);
    EEPROM.put(addr,Ti);
    addr += sizeof(Ti);
    EEPROM.put(addr,Td);
    addr += sizeof(Td);  
}

void restore_param(){
    unsigned int addr=0;
    EEPROM.get(addr,ref);
    addr += sizeof(ref);
    EEPROM.get(addr,Kp);
    addr += sizeof(Kp);
    EEPROM.get(addr,Ti);
    addr += sizeof(Ti);
    EEPROM.get(addr,Td);
    addr += sizeof(Td);  
}



